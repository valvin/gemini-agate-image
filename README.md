# Gemini Agate Image

This image allowes to publish static content locate in a git repository on [Gemini](https://gemini.circumlunar.space/). Based on [Agate](https://github.com/mbrubeck/agate) gemini server.

This images takes these environment variables:

| variable | description | default value | 
| --- | --- | --- |
| `GEMINI_DOMAIN` | domain name of the capsule | example.com |
| `GEMINI_GIT_REPO` | url of the git repository containing content | `https://gitlab.com/valvin/gemini-agate-image` |
| `GEMINI_CONTENT_FOLDER` | folder inside the git repository which contains the content. it means you have `content` folder in your repo with default | `content`|
| `GEMINI_KILN_BUILD` | if set to `yes` `kiln build` will be execute after an update of the repository | `no` |
| `GEMINI_KILN_SRC` | used when kiln build is enabled. path inside de git repository where kiln `config.toml` is located | `.` |
| `GEMINI_GIT_PATH` | path in which git repository will be cloned | `/app/data` |
| `GEMINI_CERT_PATH` | path which contains certificates. this path has to be a volume if not new certs will be generated | `/certs` |
| `GEMINI_LANG` | langage of the content | `fr-FR` |

image take those commands:

* none or `run` : launch agate after initialization process
* `update`: update repository content

initialization process is:
  * check if cert are present in `GEMINI_CERT_PATH` if not it creates it
  * clone or update the content repository
  * launch agate

docker command example:

* static files:

```
docker run --rm -it -e GEMINI_DOMAIN=mydomain.tld \
  -e GEMINI_GIT_REPO=https://gitlab.com/valvin/gemini-agate-image \
  -e GEMINI_CONTENT_FOLDER=content/static \
  -v $(pwd)/certs:/certs -p 1965:1965 \
  registry.gitlab.com/valvin/gemini-agate-image:latest
```

* kiln capsule:

```
docker run --rm -it -e GEMINI_DOMAIN=mydomain.tld \
  -e GEMINI_GIT_REPO=https://gitlab.com/valvin/gemini-agate-image \
  -e GEMINI_CONTENT_FOLDER=content/kiln/public \
  -e GEMINI_KILN_BUILD=true \
  -e GEMINI_KILN_SRC=content/kiln \
  -v $(pwd)/certs:/certs -p 1965:1965 \
  registry.gitlab.com/valvin/gemini-agate-image:latest
```
