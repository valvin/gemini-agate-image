FROM golang:alpine as build_gmnitohtml

ENV GMNITOHTML_VER 0.1.2
RUN apk add make
RUN wget -O /tmp/gmnitohtml.tar.gz https://git.sr.ht/~adnano/gmnitohtml/archive/${GMNITOHTML_VER}.tar.gz \
      && tar -xvzf /tmp/gmnitohtml.tar.gz && cd gmnitohtml-${GMNITOHTML_VER} \
      && make gmnitohtml && cp ./gmnitohtml /usr/local/bin/gmnitohtml

FROM alpine:3.16 as build_agate

ENV AGATE_VER 3.2.4

RUN apk --no-cache add cargo
RUN cargo install agate --version ${AGATE_VER}


FROM alpine:3.16
ENV GEMINI_DOMAIN example.com
ENV GEMINI_GIT_REPO https://gitlab.com/valvin/gemini-agate-image
ENV GEMINI_GIT_PATH /app/data
ENV GEMINI_CONTENT_FOLDER content
ENV GEMINI_CERT_PATH /certs
ENV GEMINI_LANG fr-FR
ENV GEMINI_PORT 1965
ENV GEMINI_KILN_BUILD no
ENV GEMINI_KILN_SRC .

RUN apk --no-cache add openssl libgcc git kiln
RUN mkdir /app
COPY --from=build_agate /root/.cargo/bin/agate /app/agate
COPY --from=build_gmnitohtml /usr/local/bin/gmnitohtml /usr/local/bin/gmnitohtml
COPY entrypoint.sh /app/entrypoint.sh

EXPOSE 1965/tcp

ENTRYPOINT ["/app/entrypoint.sh"]
CMD ["run"]
