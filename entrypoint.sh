#!/bin/sh
set -e
create_new_cert(){
  if [ ! -d "${GEMINI_CERT_PATH}" ]; then
    mkdir ${GEMINI_CERT_PATH}
  fi
  openssl req -x509 -newkey rsa:4096 -keyout ${GEMINI_CERT_PATH}/key.rsa -out ${GEMINI_CERT_PATH}/cert.pem \
    -days 3650 -nodes -subj "/CN=${GEMINI_DOMAIN}"
}

clone_repository(){
  git clone ${GEMINI_GIT_REPO} ${GEMINI_GIT_PATH}
  prepare_content
}

update_repository(){
  if [ ! -d "${GEMINI_GIT_PATH}" ]; then
    echo "Cloning repository in ${GEMINI_GIT_PATH}"
    clone_repository
  else
    cd ${GEMINI_GIT_PATH}
    git pull
    prepare_content
  fi
}

prepare_content(){
  if [  "${GEMINI_KILN_BUILD}" == "yes" ]; then
    if [ "${GEMINI_KILN_SRC}" != "." ]; then
      echo "Building content in ${GEMINI_GIT_PATH}/${GEMINI_KILN_SRC} with kiln build"
      cd ${GEMINI_GIT_PATH}/${GEMINI_KILN_SRC}
    else
      echo "Building content in ${GEMINI_GIT_PATH} with kiln build"
      cd ${GEMINI_GIT_PATH}
    fi

    kiln build
  fi
}

run_agate(){
echo "---"
echo "* content: ${GEMINI_GTI_PATH}/${GEMINI_CONTENT_FOLDER}"
echo "* content build with kiln: ${GEMINI_KILN_BUILD}"
echo "* content langage: ${GEMINI_LANG}"
echo "* domain: gemini://${GEMINI_DOMAIN}:${GEMINI_PORT} "
echo "---"

/app/agate --content ${GEMINI_GIT_PATH}/${GEMINI_CONTENT_FOLDER} \
      --certs ${GEMINI_CERT_PATH} \
      --addr [::]:${GEMINI_PORT} \
      --addr 0.0.0.0:${GEMINI_PORT} \
      --hostname ${GEMINI_DOMAIN} \
      --lang ${GEMINI_LANG}
}

default(){
  if [ ! -f "${GEMINI_CERT_PATH}/cert.pem" ]; then
    echo "No certs found in ${GEMINI_CERT_PATH}. Creates new certificate."
    create_new_cert
  fi
  
  if [ ! -d "${GEMINI_GIT_PATH}" ]; then
    echo "Cloning repository in ${GEMINI_GIT_PATH}"
    clone_repository
  else
    update_repository
  fi
  
  run_agate
}

if [ "$#" -eq "1" ]; then
  if [ "$1" = "run" ]; then
    default
  elif [ "$1" = "update" ]; then
    update_repository
  fi
else
  echo "unknown command. launching default"
  default
fi
